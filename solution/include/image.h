//
// Created by isy02 on 02.12.2021.
//

#ifndef UNTITLED_IMAGE_H
#define UNTITLED_IMAGE_H
#include <inttypes.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct pixel { uint8_t b, g, r; };

#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void delete_image(struct image* image);
struct pixel get_pixel(struct image const* image, size_t index1, size_t index2);
void set_pixel(struct image* image, struct pixel pixel, size_t index1, size_t index2);
struct image new_image(size_t width, size_t height);

#endif //UNTITLED_IMAGE_H
