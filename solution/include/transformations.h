//
// Created by isy02 on 02.12.2021.
//

#ifndef UNTITLED_ROTATOR_H
#define UNTITLED_ROTATOR_H

struct image rotate(struct image const* image);
struct image image_transform(struct image const* image, struct image (f)(struct image const*));

#endif //UNTITLED_ROTATOR_H
