//
// Created by isy02 on 02.12.2021.
//

#include <stdbool.h>
#include <stdio.h>

void print_error(const char *message) {
    fprintf(stderr, "%s\n", message);
}

void opening_check(FILE* file) {
    if (!file) print_error("File cannot found");
}

bool input_is_incorrect(int argc) {
    if (argc != 3) print_error("Incorrect input");
    return (argc != 3);
}

FILE* open_to_read(const char* filename) {
    FILE* file = fopen(filename, "rb");
    opening_check(file);//пока просто выведет ошибку, дальше на notnull файл проверяется
    return file;
}

FILE* open_to_write(const char* filename) {
    FILE* file = fopen(filename, "wb");
    opening_check(file);//пока просто выведет ошибку, дальше на notnull файл проверяется
    return file;
}
