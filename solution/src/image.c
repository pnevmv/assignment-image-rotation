//
// Created by isy02 on 02.12.2021.
//

#include "../include/image.h"
#include <inttypes.h>
#include <malloc.h>

void delete_image(struct image* image) {
    free(image->data);
}

struct pixel get_pixel(struct image const* image, size_t index1, size_t index2) {
    return image->data[(image->height - 1 - index2) * image->width + index1];
}

void set_pixel(struct image* image, struct pixel pixel, size_t index1, size_t index2) {
    image->data[index1 * image->width + index2] = pixel;
}

struct image new_image(size_t width, size_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}
