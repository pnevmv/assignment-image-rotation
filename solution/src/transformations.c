//
// Created by isy02 on 05.12.2021.
//

#include "../include/transformations.h"
#include "../include/image.h"
#include <stdlib.h>



struct image rotate(struct image const* image) {
    //Чтобы не перепутать, меняем ширину и высоту местами
    const size_t height = image->width;
    const size_t width = image->height;

    struct image result = new_image(width, height);

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            set_pixel(&result, get_pixel(image, i, j), i, j);
        }
    }

    return result;
}

struct image image_transform(struct image const* image, struct image (f)(struct image const*)) {
    return f(image);
}
