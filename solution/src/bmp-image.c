//
// Created by isy02 on 02.12.2021.
//

#include "../include/bmp-image.h"
#include "../include/image.h"
#include "../include/io-manager.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define TYPE (19778)
#define BITCOUNT (24)
#define BISIZE (40)
#define BOFFBITS (54)

uint32_t get_padding(uint32_t width) {
    return (width % 4);
}

static bool check_header( struct bmp_header* header ) {
    return (header->bfType != TYPE || header->biBitCount != BITCOUNT);
}

static bool write_data(FILE* file, struct image const* image) {
    size_t padding = get_padding(image->width);
    const size_t FILL = 0;
    for (size_t i = 0; i < image->height; i++) {
        size_t check1 = fwrite(&(image->data[i * image->width]),sizeof(struct pixel), image->width, file);
        if (check1 != image->width) return false;
        size_t check2 = fwrite(&FILL, 1, padding, file);
        if (check2 != padding) return false;
    }
    return true;
}

static bool read_data(struct bmp_header* header, FILE* file, struct image const* image) {
    for (size_t i = 0; i < header->biHeight; i++) {
        size_t check = fread(&(image->data[i * image->width]), sizeof(struct pixel), header->biWidth, file);
        if (check != header->biWidth) return false;
        fseek( file, get_padding(header->biWidth), SEEK_CUR);
    }
    return true;
}

static struct bmp_header fill_header(struct image const* image) {
    return (struct bmp_header) {
            .bfType = TYPE,//0x4D42 - бмп
            .bfileSize = (BOFFBITS +
                    image->height * image->width * sizeof(struct pixel) +
                    image->height * get_padding(image->width)),//реальный размер файла
            .bOffBits = BOFFBITS,
            .biSize = BISIZE,//размер структуры
            .biWidth = image->width,//ширина картинки
            .biHeight = image->height,//высота картинки
            .biBitCount = BITCOUNT,//24-битные пикселы
            .biSizeImage = image->height * image->width * sizeof(struct pixel) +
                    get_padding(image->width) * image->height,//размер картинки в байтах
    };
}

static struct image create_image(size_t width, size_t height) {
    struct image image;
    image.data = malloc(sizeof(struct pixel) * width * height);//память под данные
    image.width = width;
    image.height = height;
    return image;
}

static enum read_status from_bmp(FILE* file, struct image* image){
    if (!file || !image) return READ_ERROR;
    struct bmp_header* header = malloc(sizeof(struct bmp_header));//память под шапку
    size_t total = fread(header, sizeof(struct bmp_header), 1, file);
    if (total != 1 || check_header(header)) return READ_INVALID_HEADER;

    *image = create_image(header->biWidth, header->biHeight);
    if (!read_data(header, file, image)) return READ_ERROR;
    
    free(header);
    return READ_OK;
}

static enum write_status to_bmp(FILE* file, struct image const* image) {
    if (!file || !image) return WRITE_ERROR;

    struct bmp_header header = fill_header(image);//заполняем шапку
    if (check_header(&header)) return WRITE_ERROR;//проверяем шапку

    size_t total = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (total != 1) return WRITE_ERROR;

    if (!write_data(file, image)) return WRITE_ERROR;
    
    return WRITE_OK;
}

struct image read_picture(const char* path) {
    FILE* file = open_to_read(path);
    struct image image;

    switch (from_bmp(file, &image)) { //Читаем из файла в структуру
        case (READ_INVALID_HEADER):
            print_error("Invalid header read");
            return (struct image) {0};
            break;
        case (READ_ERROR):
            print_error("Invalid data read");
            return (struct image) {0};
            break;
        case (READ_OK):
            break;
    }

    //Закрываем файл
    fclose(file);

    return image;
}

bool write_picture(const char* path, struct image* result_picture) {
    FILE* new_file = open_to_write(path);

    switch (to_bmp(new_file, result_picture)) { //Записываем результат в файл
        case (WRITE_ERROR):
            print_error("Write-error");
            return false;
        default:
            return true;
    }
    //Закрываем файл
    fclose(new_file);
}
