#include "../include/image.h"
#include "../include/bmp-image.h"
#include "../include/io-manager.h"
#include "../include/transformations.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (input_is_incorrect(argc)) return 100; //Проверяем количество аргументов

    struct image picture = read_picture(argv[1]); //Исходное изображение
    struct image result_picture = image_transform(&picture, rotate); //Изображение-результат преобразований
    if (!write_picture(argv[2], &result_picture)) return 200; //Записываем в файл

    //Освобождаем память
    delete_image(&picture);
    delete_image(&result_picture);

    return 0;
}
